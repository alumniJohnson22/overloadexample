package com.mthree.training.demo;

public class OverloadDemo {

	private String ticker, exchange;
	
	public OverloadDemo(String ticker, String exchange) {
		super();
		this.ticker = ticker;
		this.exchange = exchange;
	}

	// the return type of catRIC doesn't matter to Java: this returns void
	void catRIC(String ticker,String exchange){
		System.out.println(ticker+"."+exchange);
	}

	// the return type of catRIC doesn't matter to Java: this returns String
	String catRIC(){
	    return ticker+"."+exchange;
	}
	
	public static void main(String[] args) {
		OverloadDemo demo = new OverloadDemo("VOD", "L");
		demo.catRIC("A", "B");
		System.out.println(demo.catRIC());
	}

}
